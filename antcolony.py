import time
import numpy as np

class AntPathScore:
    def __init__(self, flatten_path=[], vertices_path=[], score=float("-inf")):
        self.flatten_path = flatten_path
        self.vertices_path = vertices_path
        self.score = score

    def __repr__(self):
        return f"[{self.score}] {self.flatten_path}"

class AntColony:
    def __init__(self, cost_matrix):
        self.cost_matrix = cost_matrix
        self.n_vertices = cost_matrix.shape[0]

    def initialize_pheromone_matrix(self):
        return np.ones(self.cost_matrix.shape)

    def set_random_seed(self, seed=42):
        # np.random.seed(seed)
        pass

    def calculate_available_vertices(self, where, visited):
        return {i for i in range(0, self.n_vertices) if i != where and self.cost_matrix[i, where] > 0 and i not in visited}

    def choose_vertice(self, options, pheromone_array, fitness_array, alpha, beta):
        options = np.array(list(options))
        options_pheromones = pheromone_array[options]
        options_fitness = fitness_array[options]
        probs = options_pheromones ** alpha + options_fitness ** beta
        weights = options_pheromones / options_pheromones.sum()
        return np.random.choice(options, p=weights)

    def calculate_ant_path(self, start, end, pheromone_matrix, alpha, beta):
        ant_path = [start]
        visited = set()
        last_pos_where_can_go_to_end = 0

        while True:
            last_vertex = ant_path[-1]
            visited.add(last_vertex)
            available_vertices = self.calculate_available_vertices(last_vertex, visited)
            if len(available_vertices) == 0:
                break

            if end in available_vertices:
                last_pos_where_can_go_to_end = len(ant_path)

            selected_vertice = self.choose_vertice(available_vertices, pheromone_matrix[last_vertex], self.cost_matrix[last_vertex], alpha, beta)
            if selected_vertice == end:
                break

            ant_path.append(selected_vertice)

        true_ant_path = ant_path[:last_pos_where_can_go_to_end]
        true_ant_path.append(end)
        return true_ant_path

    def update_pheromone_matrix(self, pheromone_matrix, ant_scores, evap_factor=0.5, deposit_factor=0.5):
        # Evaporating pheromones
        pheromone_matrix *= 1 - evap_factor

        # Adding pheromones where ants walked on
        for i, ant_score in enumerate(ant_scores):
            factor = (i / len(ant_scores))**2
            pheromone_matrix[ant_score.vertices_path[:, 0], ant_score.vertices_path[:, 1]] += deposit_factor * factor * ant_score.score

        return pheromone_matrix

    def generate_iteration_stats(self, ant_scores):
        scores = np.array([ant.score for ant in ant_scores])
        return {
            "best": scores.max(),
            "worst": scores.min(),
            "mean": scores.mean(),
            "std": scores.std()
        }

    def run(self, start, end, n_ants, n_iterations, rho, Q, alpha, beta, verbose=False):
        self.set_random_seed()
        pheromone_matrix = self.initialize_pheromone_matrix()

        iteration_stats = []
        for iteration in range(n_iterations):
            if verbose: print(f" > Running iteration {iteration}.")
            iter_best_ant = AntPathScore()
            ant_scores = []
            for _ in range(n_ants):
                ant_path = self.calculate_ant_path(start, end, pheromone_matrix, alpha, beta)
                ant_path_vertices = np.array(list(zip(ant_path, ant_path[1:])))
                ant_path_cost = self.cost_matrix[ant_path_vertices[:, 0], ant_path_vertices[:, 1]].sum()
                ant_path_score = AntPathScore(ant_path, ant_path_vertices, ant_path_cost)
                ant_scores.append(ant_path_score)

                if ant_path_score.score > iter_best_ant.score:
                    iter_best_ant = ant_path_score

            ant_scores.sort(key=lambda ant: ant.score)
            pheromone_matrix = self.update_pheromone_matrix(pheromone_matrix, ant_scores, evap_factor=rho, deposit_factor=Q)
            iteration_stats.append(self.generate_iteration_stats(ant_scores))

        return iteration_stats

