import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("dfs_entrada1/f100-100-0.7-0.1-0.2-0.8.csv")
df["t"] = 0

iterations_per_run = max(df[df["run"] == 0].index) + 1
runs = max(df["run"])

values = []
for run_id in range(runs):
    df.iloc[run_id * iterations_per_run : (run_id + 1) * iterations_per_run, 6] = np.arange(0, iterations_per_run)
    

df_mean = df.groupby("t").mean()
df_std = df.groupby("t").std()
x_data = np.arange(0, iterations_per_run)

plt.axhline(y=990, color="r", linestyle="-", label="Ideal")
plt.errorbar(x_data, df_mean["best"], yerr=df_std["best"], ecolor="#3498db4d", color="#3498db", label="Melhor")
plt.errorbar(x_data, df_mean["mean"], yerr=df_std["mean"], ecolor="#2ecc714d", color="#2ecc71", label="Média")
plt.errorbar(x_data, df_mean["worst"], yerr=df_std["worst"], ecolor="#e74c3c4d", color="#e74c3c", label="Pior")

plt.legend()
plt.show()
