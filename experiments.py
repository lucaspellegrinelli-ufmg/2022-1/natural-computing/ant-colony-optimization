import os
import numpy as np
import pandas as pd
import multiprocessing

from antcolony import AntColony

# Load input file
for fid, filename in enumerate(["inputs/entrada1.txt", "inputs/entrada2.txt", "inputs/entrada3.txt"]):
    base_config = {
        "n_ants": 100,
        "n_iterations": 250,
        "rho": 0.7,
        "Q": 0.1,
        "alpha": 0.5,
        "beta": 0.5,
        "verbose": False
    }

    input_vals = np.loadtxt(filename, dtype=int)
    n_vertices = int(np.max(input_vals[:, [0, 1]]))

    def generate_config_name(config):
        n_ants = config["n_ants"]
        n_iterations = config["n_iterations"]
        rho = config["rho"]
        Q = config["Q"]
        alpha = config["alpha"]
        beta = config["beta"]
        prename = filename.split("/")[1].replace(".txt", "")

        if not os.path.exists(f"dfs_{prename}"):
            os.makedirs(f"dfs_{prename}")

        return f"dfs_{prename}/f{n_ants}-{n_iterations}-{rho}-{Q}-{alpha}-{beta}.csv"

    def run_experiment(config):
        # Create adjacency matrix that stores the cost for each edge 
        cost_matrix = np.zeros((n_vertices, n_vertices))
        cost_matrix[input_vals[:, 0] - 1, input_vals[:, 1] - 1] = input_vals[:, 2]

        all_scores = []
        for run_id in range(10):
            colony = AntColony(cost_matrix)
            scores = colony.run(start=0, end=n_vertices - 1, **config)
            for score in scores:
                score["run"] = run_id
                all_scores.append(score.copy())
        df = pd.DataFrame(all_scores)
        df.to_csv(generate_config_name(config))

    def run_n_ants_experiment():
        for n_ants in [100, 250]:
            print(f"Running n_ants = {n_ants}")
            mod_config = base_config.copy()
            mod_config["n_ants"] = n_ants
            run_experiment(mod_config)

    def run_n_iterations_experiment():
        for n_iterations in [150, 250]:
            print(f"Running n_iterations = {n_iterations}")
            mod_config = base_config.copy()
            mod_config["n_iterations"] = n_iterations
            run_experiment(mod_config)

    def run_rho_experiment():
        for rho in [0.3, 0.9]:
            print(f"Running rho = {rho}")
            mod_config = base_config.copy()
            mod_config["rho"] = rho
            run_experiment(mod_config)

    def run_alpha_experiment():
        for alpha in [0.5, 1.0, 2.0]:
            print(f"Running alpha = {alpha}")
            mod_config = base_config.copy()
            mod_config["alpha"] = alpha
            run_experiment(mod_config)

    def run_beta_experiment():
        for beta in [0.5, 1.0, 2.0]:
            print(f"Running beta = {beta}")
            mod_config = base_config.copy()
            mod_config["beta"] = beta
            run_experiment(mod_config)

    processes = [
        multiprocessing.Process(target=run_n_ants_experiment),
        multiprocessing.Process(target=run_n_iterations_experiment),
        multiprocessing.Process(target=run_rho_experiment),
        multiprocessing.Process(target=run_alpha_experiment),
        multiprocessing.Process(target=run_beta_experiment)
    ]

    for process in processes:
        process.start()

    for process in processes:
        process.join()